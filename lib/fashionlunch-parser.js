const moment = require("moment");
const { JSDOM } = require("jsdom");

module.exports = () => {
	return new Promise((resolve, reject) => {

		JSDOM.fromURL("http://swedenfashionhouse.com/veckans-meny/", {
			userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
		}).then((dom) => {
			const window = dom.window;

			const document = window.document;

			const menuTable = document.getElementById("food-menu");

			const days = [
                "monday",
                "tuesday",
                "wednesday",
                "thursday",
                "friday",
            ];

			let daysMeal = {
                monday: {
                    date: 0,
                    meals: []
                },
                tuesday: {
                    date: 0,
                    meals: []
                },
                wednesday: {
                    date: 0,
                    meals: []
                },
                thursday: {
                    date: 0,
                    meals: []   
                },
                friday: {
                    date: 0,
                    meals: []
                },
            }


			menuTable.querySelectorAll("td.food-list").forEach((foodList, i) => {
				const day = days[i];
				daysMeal[day].date = parseInt(moment().days(day).format("x"));

				const dayMeals = daysMeal[day].meals;

				foodList.querySelectorAll("p").forEach((foodItem) => {
					const foodItemRawText = foodItem.textContent;
					const firstNoBreakingSpaceIndex = foodItem.innerHTML.indexOf("&nbsp;", 1);

					let foodItemText = foodItemRawText.trim();

					if (firstNoBreakingSpaceIndex > 0) {
						foodItemText = foodItemRawText.substr(0, firstNoBreakingSpaceIndex).trim();
					}

					if (foodItemText == "") {
						return;
					}

					dayMeals.push(foodItemText);
				});
			});

			return resolve(daysMeal);
		}).catch((error) => {
			return reject(error);
		});
	});
};

